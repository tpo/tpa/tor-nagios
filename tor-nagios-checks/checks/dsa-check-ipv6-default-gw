#!/usr/bin/python3

import ipaddress
import subprocess
import sys

def get_ipv6_addrs():
    ip_output = subprocess.check_output(['ip', '-o', '-6', 'addr', 'show'])
    s = set()
    for line in ip_output.decode().splitlines():
        fields = line.split()
        if len(fields) < 4:
            continue
        ipv6addr = ipaddress.IPv6Interface(fields[3])
        if ipv6addr.is_global:
            s.add(ipv6addr)
    return s

def get_default_v6_route():
    ip_output = subprocess.check_output(['ip', '-o', '-6', 'route', 'show'])
    s = set()
    for line in ip_output.decode().splitlines():
        fields = line.split()
        if fields[0] == "default":
            return line
    return None

ipv6addrs = get_ipv6_addrs()
if len(ipv6addrs) == 0:
    print("OK: No ipv6 interfaces")
    sys.exit(0)

v6route = get_default_v6_route()
if v6route is None:
    print("Warning: No ipv6 default route")
    sys.exit(1)
else:
    print("OK:", v6route)
    sys.exit(0)
