tor-nagios-checks (39) unstable; urgency=medium

  * upgrade dh compat to 7 to fix build, unchecked
  * add prometheus support to dsa-check-backuppg
  * downgrade monitoring-plugins and nrpe deps to recommends

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 09 Oct 2024 18:26:13 -0400

tor-nagios-checks (38) UNRELEASED; urgency=medium

  * port dsa-check-statusfile check to Python 3
  * the other remaining python 2 script, dsa-check-raid-megaraid, is not
    used anywhere

 -- Antoine Beaupré <anarcat@debian.org>  Mon, 16 May 2022 21:45:53 -0400

tor-nagios-checks (37) unstable; urgency=medium

  * add check_md3000i.pl from Nagios Exchange

 -- Jerome Charaoui <jerome@riseup.net>  Wed, 08 Sep 2021 12:51:51 -0400

tor-nagios-checks (36) unstable; urgency=medium

  * port Nagios checks to bullseye

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 26 Aug 2021 21:03:43 -0400

tor-nagios-checks (35) unstable; urgency=medium

  * Add dsa-check-raid-megaraid-sas from debian.

 -- Peter Palfrader <weasel@debian.org>  Mon, 23 Nov 2020 23:50:10 +0100

tor-nagios-checks (34) unstable; urgency=medium

  * improve error messages in ganeti checks

 -- Antoine Beaupré <anarcat@debian.org>  Tue, 23 Jun 2020 17:27:19 -0400

tor-nagios-checks (33) unstable; urgency=medium

  * avoid conflict with existing check_cluster command from
    monitoring-plugins-basic

 -- Antoine Beaupré <anarcat@debian.org>  Tue, 23 Jun 2020 17:00:19 -0400

tor-nagios-checks (32) unstable; urgency=medium

  [ Antoine Beaupré ]
  * noop. Add (#34111) then remove (#34240) torpert monitoring in
    collector check.
  * add Ganeti checks (#33810)

  [ Peter Palfrader ]
  * dsa-check-mirrorsync: update from Debian

 -- Antoine Beaupré <anarcat@debian.org>  Tue, 23 Jun 2020 16:47:10 -0400

tor-nagios-checks (31) unstable; urgency=medium

  * more arguments to collector check (#34029)

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 29 Apr 2020 13:51:34 -0400

tor-nagios-checks (30) unstable; urgency=medium

  * tor-check-collector: add Karsten's collector check (#33972)

 -- Antoine Beaupré <anarcat@debian.org>  Mon, 27 Apr 2020 10:50:25 -0400

tor-nagios-checks (29) unstable; urgency=medium

  * clarify messages on outdated chroots

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 05 Feb 2020 16:29:37 -0500

tor-nagios-checks (28) unstable; urgency=medium

  [ Peter Palfrader ]
  * dsa-check-raid-sw: update from Debian
  * dsa-check-drbd: add from Debian

 -- Antoine Beaupré <anarcat@debian.org>  Tue, 13 Aug 2019 14:06:40 -0400

tor-nagios-checks (27) unstable; urgency=medium

  * dsa-check-backuppg: update from Debian
  * dsa-check-ucode-intel: add from Debian

 -- Peter Palfrader <weasel@debian.org>  Mon, 20 May 2019 14:58:00 +0200

tor-nagios-checks (26) unstable; urgency=medium

  [ Iain R. Learmonth ]
  * tor-check-onionoo: add optional SSL switch and hostname/port argument.

  [ Peter Palfrader ]
  * dsa-check-running-kernel: handle -unsigned packages

  [ Antoine Beaupré ]
  * check_puppetdb_nodes: add from https://github.com/evgeni/check_puppetdb_nodes

 -- Antoine Beaupré <anarcat@debian.org>  Fri, 08 Mar 2019 10:45:52 -0500

tor-nagios-checks (25) unstable; urgency=medium

  * dsa-check-unbound-anchors: add from debian

 -- Peter Palfrader <weasel@debian.org>  Thu, 11 Oct 2018 09:48:39 +0200

tor-nagios-checks (24) unstable; urgency=medium

  * dsa-check-backuppg: update from debian
  * dsa-check-dchroots-current: add from debian
  * dsa-check-backuppg: update from debian
  * dsa-check-ipv6-default: add from debian

 -- Peter Palfrader <weasel@debian.org>  Wed, 28 Mar 2018 11:50:57 +0200

tor-nagios-checks (22) unstable; urgency=medium

  * dsa-check-mirrorsync: update from debian
  * dsa-check-zone-signature-all: copy from debian
  * dsa-check-dnssec-delegation: update from debian
  * dsa-check-running-kernel: update from debian

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Nov 2016 15:42:40 +0100

tor-nagios-checks (21) unstable; urgency=medium

  * Add dsa-check-filesystems from debian
  * Add dsa-check-cert-expire-dir from debian
  * Update dsa-check-zone-rrsig-expiration-many from debian
  * Update dsa-check-zone-rrsig-expiration from debian
  * Depend on monitoring-plugins-basic as an alternative to nagios-plugins-basic.

 -- Peter Palfrader <weasel@debian.org>  Mon, 06 Jun 2016 14:19:32 +0200

tor-nagios-checks (20) unstable; urgency=medium

  * dsa-check-bacula: sync from debian
  * Add dsa-check-stunnel-sanity

 -- Peter Palfrader <weasel@debian.org>  Mon, 06 Jun 2016 14:16:47 +0200

tor-nagios-checks (19) unstable; urgency=medium

  * Depend on libyaml-syck-perl.

 -- Peter Palfrader <weasel@debian.org>  Thu, 29 Jan 2015 09:29:21 +0100

tor-nagios-checks (18) unstable; urgency=low

  * better bacula checks.
  * dsa-check-backuppg: sync from debian.
  * dsa-check-dnssec-delegation: sync from Debian.
  * dsa-check-zone-rrsig-expiration: sync from Debian.
  * dsa-check-zone-rrsig-expiration-many: sync from Debian.
  * tor-check-onionoo: Add karsten's onionoo check.
  * dsa-check-libs: add.

 -- Peter Palfrader <weasel@debian.org>  Thu, 29 Jan 2015 09:26:21 +0100

tor-nagios-checks (17) unstable; urgency=low

  * Update existing checks from debian.
  * Add bacula checks.

 -- Peter Palfrader <weasel@debian.org>  Tue, 19 Mar 2013 14:26:54 +0100

tor-nagios-checks (16) unstable; urgency=low

  * New dsa-check-packages.

 -- Peter Palfrader <weasel@debian.org>  Sun, 30 Dec 2012 13:13:41 +0100

tor-nagios-checks (15) unstable; urgency=low

  * dsa-update-apt-status: copy update from debian to exit with 0 if all is
    well.

 -- Peter Palfrader <weasel@debian.org>  Thu, 20 Dec 2012 17:06:51 +0100

tor-nagios-checks (14) unstable; urgency=low

  * dsa-check-backuppg: Ignore .dotfiles in rootdir.
  * dsa-check-running-kernel: Update from debian.

 -- Peter Palfrader <weasel@debian.org>  Sat, 25 Aug 2012 18:19:38 +0200

tor-nagios-checks (13) unstable; urgency=low

  * Add dsa-check-backuppg*.
  * Update dsa-check-zone-rrsig-expiration (configurable packet size).
  * Add dsa-check-port-closed.
  * dsa-check-udldap-freshness: check new last_update.trace instead
    of passwd.tdb.
  * rename checks/dsa-check-backuppg.conf.sample ->
    etc/dsa-check-backuppg.conf.sample.

 -- Peter Palfrader <weasel@debian.org>  Sat, 10 Mar 2012 20:40:30 +0100

tor-nagios-checks (12) unstable; urgency=low

  * Get rid of nrpe config from package.

 -- Peter Palfrader <weasel@debian.org>  Sat, 08 Oct 2011 16:35:27 +0200

tor-nagios-checks (11) unstable; urgency=low

  * Update nrpe config.

 -- Peter Palfrader <weasel@debian.org>  Fri, 15 Jul 2011 15:01:33 +0200

tor-nagios-checks (10) unstable; urgency=low

  * Update nrpe config.

 -- Peter Palfrader <weasel@debian.org>  Wed, 23 Feb 2011 19:29:27 +0100

tor-nagios-checks (9) unstable; urgency=low

  * Update nrpe config.

 -- Peter Palfrader <weasel@debian.org>  Wed, 23 Feb 2011 19:22:48 +0100

tor-nagios-checks (8) unstable; urgency=low

  * Update nrpe config.

 -- Peter Palfrader <weasel@debian.org>  Tue, 15 Feb 2011 17:09:24 +0100

tor-nagios-checks (7) unstable; urgency=low

  * Update nrpe config.

 -- Peter Palfrader <weasel@debian.org>  Fri, 01 Oct 2010 13:21:17 +0200

tor-nagios-checks (6) unstable; urgency=low

  * Update nrpe config.

 -- Peter Palfrader <weasel@debian.org>  Sat, 24 Jul 2010 11:32:33 +0200

tor-nagios-checks (5) unstable; urgency=low

  * Update nrpe config.

 -- Peter Palfrader <weasel@debian.org>  Thu, 06 May 2010 20:07:34 +0200

tor-nagios-checks (4) unstable; urgency=low

  * actually *re*start nrpe in postinst.
  * Update nrpe config.

 -- Peter Palfrader <weasel@debian.org>  Tue, 04 May 2010 23:53:12 +0200

tor-nagios-checks (3) unstable; urgency=low

  * Update nrpe config.

 -- Peter Palfrader <weasel@debian.org>  Tue, 04 May 2010 23:42:33 +0200

tor-nagios-checks (2) unstable; urgency=low

  * Restart nagios-nrpe-server in postinst.
  * Do not run weak-keycheck in postinst or from cron.
  * Fix path for etc-nrpe.d in debian/rules.
  * Refuse to build without nrpe_tor.cfg.

 -- Peter Palfrader <weasel@debian.org>  Tue, 04 May 2010 22:50:59 +0200

tor-nagios-checks (1) unstable; urgency=low

  * Fork for tor.
  * Rename to tor-*.
  * Do not depend on dsa-ssh-weak-keys.
  * Ship some nrpe config files with this package.

 -- Peter Palfrader <weasel@debian.org>  Tue, 04 May 2010 22:36:14 +0200

dsa-nagios-checks (8X) Xnstable; urgency=low

  [ Peter Palfrader ]
  * Add dsa-check-raid-megaraid.
  * make samhain check go WARN, not CRITICAL.

  [ Stephen Gran ]
  * Add dsa-check-uptime

  [ Uli Martens ]
  * dsa-check-running-kernel:
    - Teach dsa-check-running-kernel about LZMA compressed kernel images.
    - Handle cases where a meta package depends on a list of packages
      (such as image and module package).

 -- Peter Palfrader <weasel@debian.org>  Tue, 04 May 2010 20:31:29 +0200

dsa-nagios-checks (85) unstable; urgency=low

  [ Stephen Gran ]
  * dsa-update-apt-status: Fix bashism by making it a bash script
  [ Peter Palfrader ]
  * dsa-check-hpasm: Not all things where a temperature can be measured
    might be installed (e.g. a second CPU).  Don't do numeric computations
    on a "-" instead of numbers then.
  * dsa-check-hpasm: Support a --ps-no-redundant option.  If supplied
    then non-redundant power supply will not be a warning state.
  * dsa-check-hpasm: Support a --fan-no-redundant option.  If supplied
    then non-redundant fans will not be a warning state.
  * Add dsa-check-msa-eventlog.
  * add dsa-check-zone-rrsig-expiration (therefore recommend
    libnet-dns-sec-perl).
  * add dsa-check-zone-rrsig-expiration-many.
  * dsa-check-raid-aacraid: properly support beethoven's Adaptec AAC-RAID
    controller with a battery: "ZMM Optimal" is the way it says "optimal".
  * dsa-check-dabackup-server: check /etc/ssh/userkeys/root instead of
    /root/.ssh/authorized_keys.
  * add dsa-check-dnssec-delegation.
  * dsa-check-hpasm: Add --fan-high to not tread high fan speeds as a
    warning condition.
  * weak-ssh-keys-check: Check all keys in files in /etc/ssh/userkeys
    and /var/lib/misc/userkeys, not just the first.  (Also doesn't
    blow up if a file is empty.)

 -- Peter Palfrader <weasel@debian.org>  Mon, 22 Feb 2010 10:32:51 +0100

dsa-nagios-checks (84) unstable; urgency=low

  [ Peter Palfrader ]
  * dsa-check-mirrorsync: Handle case where we cannot parse a tracefile
    better (do not reference undefined values, properly exit with UNKNOWN).
  * dsa-check-mirrorsync: Fix counting of out-of-date mirrors.
  * dsa-check-dabackup-server: Handle wildcard entries (via the new ssh
    wrapper) in the authorized_keys files.  Such entries accept all
    backup-manager entries for their host, but they do not require one.
    As such we can just put all the hosts in the authkeys file regardless of
    whether we actually have backups configured for them.

  [ Stephen Gran ]
  * New check: dsa-check-hpasm
  * Release for new check

 -- Stephen Gran <sgran@debian.org>  Mon, 30 Nov 2009 22:10:46 +0000

dsa-nagios-checks (83) stable; urgency=low

  * Add dsa-check-cert-expire: Checks if a given cert on disk will expire
    soon.

 -- Peter Palfrader <weasel@debian.org>  Fri, 02 Oct 2009 20:22:42 +0200

dsa-nagios-checks (82) stable; urgency=low

  * dsa-check-raid-3ware: Learn about REBUILDING state for 3ware raid units.

 -- Peter Palfrader <weasel@debian.org>  Thu, 20 Aug 2009 13:48:43 +0200

dsa-nagios-checks (81) stable; urgency=low

  * dsa-check-raid-mpt: mpt-status returns non-zero exit code when
    it found degraded raid arrays.  Handle this correctly.

 -- Peter Palfrader <weasel@debian.org>  Fri, 07 Aug 2009 12:17:22 +0200

dsa-nagios-checks (80) stable; urgency=low

  * dsa-check-packages: In the short overview say "obs/loc" instead of
    just "obs" for local or obsolete packages.
  * dsa-update-apt-status: Introduce -f switch.

 -- Peter Palfrader <weasel@debian.org>  Wed, 22 Jul 2009 11:34:15 +0200

dsa-nagios-checks (79) stable; urgency=low

  * dsa-check-running-kernel: Correctly check *candidate* version
    of kernel metapackage for unsatisfied depends, not the latest
    metapackage (which might come from a non-default source).

 -- Peter Palfrader <weasel@debian.org>  Fri, 03 Jul 2009 20:07:13 +0200

dsa-nagios-checks (78) stable; urgency=low

  * dsa-check-raid-aacraid: tmp directory in /tmp,
  * dsa-check-raid-aacraid: unlink UcliEvt.log so tmpdir cleanup
    doesn't fail.

 -- Peter Palfrader <weasel@debian.org>  Sun, 28 Jun 2009 16:36:39 +0200

dsa-nagios-checks (77) stable; urgency=low

  * Add checks/dsa-check-raid-aacraid.

 -- Peter Palfrader <weasel@debian.org>  Sun, 28 Jun 2009 16:31:13 +0200

dsa-nagios-checks (76) stable; urgency=low

  * Ship with an /etc/nagios/obsolete-packages-ignore.d directory.

 -- Peter Palfrader <weasel@debian.org>  Sun, 14 Jun 2009 12:09:54 +0200

dsa-nagios-checks (75) stable; urgency=low

  * dsa-check-packages: Support ignore directories.
  * debian/control: Fix dup word in description.

 -- Peter Palfrader <weasel@debian.org>  Sun, 14 Jun 2009 12:01:00 +0200

dsa-nagios-checks (74) stable; urgency=low

  * -e is sufficient

 -- Stephen Gran <sgran@debian.org>  Thu, 21 May 2009 22:12:34 +0100

dsa-nagios-checks (73) stable; urgency=low

  * Er, new test needs to be executable

 -- Stephen Gran <sgran@debian.org>  Thu, 21 May 2009 21:17:27 +0100

dsa-nagios-checks (72) stable; urgency=low

  * Generalize puppet file age check and remove old one

 -- Stephen Gran <sgran@debian.org>  Thu, 21 May 2009 20:52:12 +0100

dsa-nagios-checks (71) stable; urgency=low

  * Update the kernel check to not complain when we can't trace back to a meta
    package name - this generally indicates a custom kernel that we're running
    on purpose.

 -- Stephen Gran <sgran@debian.org>  Tue, 19 May 2009 22:20:44 +0100

dsa-nagios-checks (70) XXstable; urgency=low

  * No longer check nrpe config in postinst - puppet fixes it for us anyway.
  * Only run weak keys check if there is no prior weak key status.
  * dsa-check-hpacucli: Slots need not be all numeric.
  * dsa-check-hpacucli: Spares can be active.
  * dsa-check-hpacucli: And SAS can be E too.

 -- Peter Palfrader <weasel@debian.org>  Sat, 18 Apr 2009 01:41:32 +0200

dsa-nagios-checks (69) unstable; urgency=low

  * Rename to dsa-nagios-checks.
  * Replaces/Conflicts dsa-nagios-nrpe-config.
  * Update description.
  * No longer install nrpe_dsa.cfg.
  * Move files into specific directories in source.

 -- Peter Palfrader <weasel@debian.org>  Tue, 31 Mar 2009 19:19:14 +0200

dsa-nagios-nrpe-config (68) unstable; urgency=low

  * dsa-update-apt-status: Ensure apt-get update is quiet even when stuff
    goes wrong.

 -- Peter Palfrader <weasel@debian.org>  Wed, 11 Feb 2009 21:59:55 +0100

dsa-nagios-nrpe-config (67) unstable; urgency=low

  * Minor tweak to dsa-check-packages:  reorder ok and obs(ign), change
    long text for obs(ign).

 -- Peter Palfrader <weasel@debian.org>  Wed,  4 Feb 2009 09:40:26 +0000

dsa-nagios-nrpe-config (66) unstable; urgency=low

  * Add dsa-check-packages
  * Make apt-status-check use dsa-check-packages.
  * And install obsolete-packages-ignore into etc/nagios.

 -- Peter Palfrader <weasel@debian.org>  Tue, 03 Feb 2009 20:39:01 +0100

dsa-nagios-nrpe-config (65) unstable; urgency=low

  * Add dsa-check-soas

 -- Peter Palfrader <weasel@debian.org>  Tue, 23 Dec 2008 23:11:19 +0000

dsa-nagios-nrpe-config (64) unstable; urgency=low

  * dsa-check-mirrorsync: make it work in embedded perl again.

 -- Peter Palfrader <weasel@debian.org>  Tue, 23 Dec 2008 22:59:06 +0000

dsa-nagios-nrpe-config (63) unstable; urgency=low

  * dsa-check-mirrorsync: print help if additional args are given (we don't
    support any)

 -- Peter Palfrader <weasel@debian.org>  Tue, 23 Dec 2008 22:14:44 +0000

dsa-nagios-nrpe-config (62) unstable; urgency=low

  * Add check_securitymirror by formorer.

 -- Peter Palfrader <weasel@debian.org>  Tue, 23 Dec 2008 21:59:47 +0000

dsa-nagios-nrpe-config (61) unstable; urgency=low

  * dsa-check-hpacucli: Do not create the 'Failed' array while checking if an
    entry is in there.  If it doesn't exist that's also good.

 -- Peter Palfrader <weasel@debian.org>  Mon, 06 Oct 2008 14:08:30 +0200

dsa-nagios-nrpe-config (60) unstable; urgency=low

  * Add dsa-check-raid-areca.

 -- Peter Palfrader <weasel@debian.org>  Wed, 01 Oct 2008 16:23:33 +0200

dsa-nagios-nrpe-config (59) unstable; urgency=low

  * apt-status-check: Ignore stderr during apt-get update.

 -- Peter Palfrader <weasel@debian.org>  Tue, 30 Sep 2008 19:58:36 +0200

dsa-nagios-nrpe-config (58) unstable; urgency=low

  * Rename dsa-check-da-in-aliases to dsa-check-config.
  * dsa-check-config: check if ldap.conf is configured correctly.

 -- Peter Palfrader <weasel@debian.org>  Fri, 26 Sep 2008 15:02:53 +0200

dsa-nagios-nrpe-config (57) unstable; urgency=low

  * dsa-check-hpacucli: Do not inspect drives in detail when they are
    listed as Failed in the overview already.

 -- Peter Palfrader <weasel@debian.org>  Wed, 24 Sep 2008 16:02:31 +0200

dsa-nagios-nrpe-config (56) unstable; urgency=low

  * fix dsa-check-samhain.
    sudo is not in $PATH.

 -- Martin Zobel-Helas <zobel@debian.org>  Mon, 22 Sep 2008 06:45:19 +0000

dsa-nagios-nrpe-config (55) unstable; urgency=low

  * And another one, weasel should sleep.

 -- Peter Palfrader <weasel@debian.org>  Mon, 22 Sep 2008 00:29:15 +0000

dsa-nagios-nrpe-config (54) unstable; urgency=low

  * dsa-check-dabackup-server fixes.

 -- Peter Palfrader <weasel@debian.org>  Mon, 22 Sep 2008 00:27:41 +0000

dsa-nagios-nrpe-config (53) unstable; urgency=low

  * Add dsa-check-dabackup-server.

 -- Peter Palfrader <weasel@debian.org>  Mon, 22 Sep 2008 00:19:28 +0000

dsa-nagios-nrpe-config (52) unstable; urgency=low

  * The nagios _check_ package should probably not be the one that
    depends on the stuff we want on most or all hosts, thus removing
    dependency on samhain, which should probably get pulled in via
    debian.org.

 -- Peter Palfrader <weasel@debian.org>  Sun, 21 Sep 2008 23:41:01 +0000

dsa-nagios-nrpe-config (51) unstable; urgency=low

  * Add check for samhain.

 -- Martin Zobel-Helas <zobel@samosa.debian.org>  Sat, 20 Sep 2008 07:29:23 +0000

dsa-nagios-nrpe-config (50) unstable; urgency=low

  * dsa-check-hpacucli: Handle 'pd all show' output with failed arrays.

 -- Peter Palfrader <weasel@debian.org>  Sat, 09 Aug 2008 11:39:55 +0200

dsa-nagios-nrpe-config (49) unstable; urgency=low

  * dsa-check-hpacucli: Learn correct transfer speed for Ultra 3 Wide (160mb/s)

 -- Peter Palfrader <weasel@debian.org>  Wed, 23 Jul 2008 00:56:54 +0200

dsa-nagios-nrpe-config (48) unstable; urgency=low

  * Don't just print the sudoers lines to stdout, tell the admin to add them
    also.

 -- Peter Palfrader <weasel@debian.org>  Tue, 22 Jul 2008 21:25:56 +0000

dsa-nagios-nrpe-config (47) unstable; urgency=low

  * dsa-check-hpacucli: Check transfer speed of disks.

 -- Peter Palfrader <weasel@debian.org>  Tue, 22 Jul 2008 23:14:50 +0200

dsa-nagios-nrpe-config (46) unstable; urgency=low

  * Move apt status update from cron.daily to usr/sbin/dsa-update-apt-status.
  * Move script calls from cron.daily to cron.d @daily and @reboot.
  * Introduce hourly runs for dsa-update-apt-status that happen if the system
    changed in the meantime, that is if /var/lib/dpkg/status or
    /var/cache/apt/pkgcache.bin are newer than the status file.
    Also run if the status file does not exist, or if the last apt-get update
    call failed, or if the last run was more than a day ago.
  * No longer do @daily runs of dsa-update-apt-status

 -- Peter Palfrader <weasel@debian.org>  Sun, 06 Jul 2008 11:51:27 +0200

dsa-nagios-nrpe-config (45) unstable; urgency=low

  * Add apt upgrades check.

 -- Peter Palfrader <weasel@debian.org>  Tue, 20 May 2008 22:01:07 +0000

dsa-nagios-nrpe-config (44) unstable; urgency=low

  * Fix ssh-dss detection.  It need not be at the start of a line
    (think >from="1.2.3.4" ssh-dss< ...).

 -- Peter Palfrader <weasel@debian.org>  Mon, 19 May 2008 14:34:23 +0000

dsa-nagios-nrpe-config (43) unstable; urgency=low

  * Depend on ruby.

 -- Peter Palfrader <weasel@debian.org>  Mon, 19 May 2008 14:19:10 +0000

dsa-nagios-nrpe-config (42) unstable; urgency=low

  * weak-ssh-keys-check: Ignore empty lines in authorized-keys files.
  * weak-ssh-keys-check: Handle servers not doing DSA in from_ssh_host().
  * Run weak-ssh-keys-check on install.

 -- Peter Palfrader <weasel@debian.org>  Mon, 19 May 2008 14:05:10 +0000

dsa-nagios-nrpe-config (41) unstable; urgency=low

  * Add dsa-check-statusfile.
  * Add weak-ssh-keys-check to be run from cron.daily.  Depend on
    libberkeleydb-perl and dsa-ssh-weak-keys now.
  * Remove up /var/cache/dsa/nagios/weak-ssh-keys in postrm purge.

 -- Peter Palfrader <weasel@debian.org>  Mon, 19 May 2008 15:55:43 +0200

dsa-nagios-nrpe-config (40) unstable; urgency=low

  * Teach dsa-check-hpacucli about rebuilding.

 -- Peter Palfrader <weasel@debian.org>  Fri,  9 May 2008 12:55:07 +0000

dsa-nagios-nrpe-config (39) unstable; urgency=low

  * Add dsa-check-hpacucli.
  * Suggest hpacucli.

 -- Peter Palfrader <weasel@debian.org>  Wed,  7 May 2008 19:54:55 +0000

dsa-nagios-nrpe-config (38) unstable; urgency=low

  * Try harder to find version string.

 -- Peter Palfrader <weasel@debian.org>  Mon,  5 May 2008 17:48:42 +0000

dsa-nagios-nrpe-config (37) unstable; urgency=low

  * dsa-check-running-kernel: also check /boot/vmlinux-`uname -r`
  * Depend on binutils for string(1).

 -- Peter Palfrader <weasel@debian.org>  Mon,  5 May 2008 17:41:13 +0000

dsa-nagios-nrpe-config (36) unstable; urgency=low

  * Add dsa-check-running-kernel

 -- Peter Palfrader <weasel@debian.org>  Mon,  5 May 2008 17:11:54 +0000

dsa-nagios-nrpe-config (35) unstable; urgency=low

  * dsa-check-raid-mpt

 -- Peter Palfrader <weasel@debian.org>  Tue, 29 Apr 2008 12:57:50 +0000

dsa-nagios-nrpe-config (34) unstable; urgency=low

  * dsa-check-raid-3ware

 -- Peter Palfrader <weasel@debian.org>  Thu, 24 Apr 2008 19:37:47 +0000

dsa-nagios-nrpe-config (33) unstable; urgency=low

  * Add dsa-check-raid-dac960

 -- Peter Palfrader <weasel@debian.org>  Thu, 24 Apr 2008 19:20:57 +0000

dsa-nagios-nrpe-config (32) unstable; urgency=low

  * Add dsa-check-udldap-freshness.

 -- Peter Palfrader <weasel@debian.org>  Wed, 16 Apr 2008 07:39:21 -0400

dsa-nagios-nrpe-config (31) unstable; urgency=low

  * dsa-check-dabackup: Use last time's log file if backup is currently running.

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Apr 2008 17:37:54 -0400

dsa-nagios-nrpe-config (30) unstable; urgency=low

  * dsa-check-raid-sw: cleaner output.

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Apr 2008 17:05:17 -0400

dsa-nagios-nrpe-config (29) unstable; urgency=low

  * dsa-check-dabackup: terser output.

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Apr 2008 13:09:07 -0400

dsa-nagios-nrpe-config (28) unstable; urgency=low

  * dsa-check-dabackup: ignore .bak files in confdir.

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Apr 2008 12:23:25 -0400

dsa-nagios-nrpe-config (27) unstable; urgency=low

  * Change OK text when da-backup not installed.

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Apr 2008 12:19:00 -0400

dsa-nagios-nrpe-config (26) unstable; urgency=low

  * dsa-check-dabackup: Warn if installed but no backups configured.

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Apr 2008 12:13:30 -0400

dsa-nagios-nrpe-config (25) unstable; urgency=low

  * Install dsa-check-dabackup

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Apr 2008 12:05:11 -0400

dsa-nagios-nrpe-config (24) unstable; urgency=low

  * Check for dsa-check-backup sudoers entry

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Apr 2008 11:48:48 -0400

dsa-nagios-nrpe-config (23) unstable; urgency=low

  * Add dsa-check-dabackup

 -- Peter Palfrader <weasel@debian.org>  Mon, 14 Apr 2008 10:56:32 -0400

dsa-nagios-nrpe-config (22) unstable; urgency=low

  * Add a very easy dsa-check-da-in-aliases.

 -- Peter Palfrader <weasel@debian.org>  Fri, 11 Apr 2008 10:25:54 -0400

dsa-nagios-nrpe-config (21) unstable; urgency=low

  * Add dsa-check-raid-sw.

 -- Peter Palfrader <weasel@debian.org>  Thu,  3 Apr 2008 06:22:44 -0400

dsa-nagios-nrpe-config (20) unstable; urgency=low

  * Do away with a new changelog entry and version for every build of the
    package - it floods us with useless commit mails - and just
    increase the version to $TIMESTAMP in the build area.

 -- Peter Palfrader <weasel@debian.org>  Thu,  3 Apr 2008 05:38:27 -0400

dsa-nagios-nrpe-config (1) unstable; urgency=low

  * Initial release.

 -- Peter Palfrader <weasel@debian.org>  Wed,  2 Apr 2008 22:24:12 +0200
